import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Pagination from 'react-bootstrap/Pagination';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Spinner from 'react-bootstrap/Spinner';
import caretUpFill from 'bootstrap-icons/icons/caret-up-fill.svg';
import caretDownFill from 'bootstrap-icons/icons/caret-down-fill.svg';
import Modal from 'react-bootstrap/Modal';
import { useLocation, useNavigate } from 'react-router-dom';
import { useLogout } from '../hooks/useLogout';
import { useAuth } from '../AuthProvider';
import EditModal from '../components/EditModal';
import Alert from 'react-bootstrap/Alert';


function CertificatesPage() {
    const location = useLocation();
    const [selectedCertificate, setSelectedCertificate] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const [search, setSearch] = useState('');
    const [data, setData] = useState();
    const [selectedValue, setSelectedValue] = useState('Items per Page');
    const [sortOrder, setSortOrder] = useState({});
    const [loading, setLoading] = useState(true);
    const [links, setLinks] = useState({});
    const [showCreateModal, setShowCreateModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [newTag, setNewTag] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [error, setError] = useState('');
    const [searchParams, setSearchParams] = useState({
        tags: [],
        searchText: '',
        sortByDate: '',
        sortByName: '',
        'page.size': '',
        'page.afterId': '',
        'page.direction': '',
    });
    const [itialPageLoad, setItialPageLoad] = useState(true);
    const navigate = useNavigate();
    const searchParamsToString = () => {
        let params = '';
        for (let key in searchParams) {
            const value = searchParams[key];
            if (Array.isArray(value)) {
                value.forEach(val => {
                    params += `&${key}=${val}`;
                });
            } else if (value) {
                params += `&${key}=${value}`;
            }
        }
        return params ? `?${params.slice(1)}` : '';
    };

    const getParamsFromUrl = (url) => {
        let params = {};
        const queryString = new URL(url).search;
        new URLSearchParams(queryString).forEach((value, key) => {
            params[key] = value;
        });
        return params;
    };

    const handlePagination = (link) => {
        console.log('handlePagination');
        if (link) {
            const params = getParamsFromUrl(link.href);
            setSearchParams(prev => ({ ...prev, 'page.afterId': params['page.afterId'], 'page.direction': params['page.direction'] }));
        }
    };

    useEffect(() => {
        console.log('initial useeffect called');
        const params = getParamsFromUrl(window.location.href);

        let searchFromParams = params.searchText || '';
        if (params.tags && params.tags.length > 0) {
            searchFromParams += ' #' + params.tags.join(' #');
        }

        if ([10, 20, 50].includes(parseInt(params['page.size']))) {
            setSelectedValue(params['page.size']);
        }

        if (['ASC', 'DESC'].includes(params.sortByDate)) {
            sortOrder['sortByDate'] = params.sortByDate;
        }

        if (['ASC', 'DESC'].includes(params.sortByName)) {
            sortOrder['sortByName'] = params.sortByName;
        }

        setSearch(searchFromParams);
        setSearchParams(prev => ({ ...prev, ...params }));
    }, []);

    useEffect(() => {
        console.log('useEffect called searchParams');
        console.log(searchParams);
        const searchParamsString = searchParamsToString();
        if (itialPageLoad) {
            setItialPageLoad(false);
        } else {
            navigate(`/certificates${searchParamsString}`);
        }
    }, [searchParams]);

    useEffect(() => {
        console.log('useEffect called location search');
        console.log(location.search)
        handleSearch();
    }, [location.search]);

    const { getAccessToken } = useAuth();

    const logout = useLogout();

    const generateApiUrl = () => {
        const baseApiUrl = 'http://localhost:8080/api/certificates';
        const searchParams = location.search;

        if (searchParams) {
            return `${baseApiUrl}${searchParams}`;
        }
        return baseApiUrl;
    };

    const handleSearch = async () => {
        console.log('Performing search');
        const token = await getAccessToken();
        if (!token) {
            logout();
            return;
        }

        const apiUrl = generateApiUrl();
        setLoading(true);
        try {
            const response = await fetch(apiUrl, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            if (!response.ok) {
                const error = await response.json();
                let errorMessage
                if (response.status >= 500) {
                    console.error('Error:', "HTTP error " + response.status + " - " + JSON.stringify(error));
                    errorMessage = "Oops! Something went wrong, please try again later.";
                    setError(errorMessage);
                    setShowAlert(true);
                } else {
                    errorMessage = "HTTP error " + response.status + " - " + JSON.stringify(error);
                    throw new Error(errorMessage);
                }
            } else {
                const data = await response.json();
                setData(data);
                setLinks(data._links || {});
                setLoading(false);
            }

        } catch (error) {
            console.error('Error:', error);
            setError(error.message);
            setShowAlert(true);
            setLoading(false);
        } finally {
            setLoading(false);
        }
    };

    const handleSelect = (value) => {
        console.log('handleSelect');
        setSearchParams(prev => ({ ...prev, 'page.size': value }));
        setSelectedValue(value);
    };

    const handleSort = (key) => {
        setSortOrder(prevState => ({
            ...prevState,
            [key]: prevState[key] === 'DESC' ? 'ASC' : (prevState[key] === 'ASC' ? 'both' : 'DESC')
        }));
        console.log('hendlesort');
        setSearchParams(prev => {
            const newSortOrder = sortOrder[key] === 'DESC' ? 'ASC' : (sortOrder[key] === 'ASC' ? null : 'DESC');
            console.log('set sort order' + newSortOrder);
            return {
                ...prev,
                [key]: newSortOrder,
                'page.afterId': '',
                'page.direction': '',
            };
        });
    };

    const handleSearchInput = (e) => {
        console.log('handlesearch');
        e.preventDefault();
        const searchText = search.split('#').map((s) => s.trim());
        const tags = searchText.splice(1).map((tag) => tag.replace(/(\(|\))/g, ''));
        setSearchParams((prev) => ({
            ...prev,
            searchText: searchText[0],
            tags: tags,
            'page.afterId': '',
            'page.direction': '',
        }));
    };

    const handleView = (item) => {
        setSelectedCertificate(item);
        setShowModal(true);
    };

    const handleCloseModal = (callback) => {
        setSelectedCertificate(null);
        callback();
    };

    const handleEdit = (certificate) => {
        setSelectedCertificate(certificate);
        setShowEditModal(true);
    };

    const handleEditSave = async () => {
        const token = await getAccessToken();
        if (!token) {
            logout();
            return;
        }

        const baseApiUrl = 'http://localhost:8080/api/certificates';
        const apiUrl = `${baseApiUrl}/${selectedCertificate.id}`;

        const requestBody = {
            name: selectedCertificate.name,
            description: selectedCertificate.description,
            price: parseFloat(selectedCertificate.price),
            duration: parseInt(selectedCertificate.duration),
            tags: selectedCertificate.tags.map(tag => {
                if (tag.id) {
                    return { id: tag.id, name: tag.name };
                } else {
                    return { name: tag.name };
                }
            })
        };

        console.log(requestBody);

        try {
            const response = await fetch(apiUrl, {
                method: 'PATCH',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            });
            if (!response.ok) {
                const error = await response.json();
                let errorMessage
                if (response.status >= 500) {
                    errorMessage = "Oops! Something went wrong, please try again later.";
                    setError(errorMessage);
                    setShowAlert(true);
                } else {
                    errorMessage = "HTTP error " + response.status + " - " + JSON.stringify(error);
                    throw new Error(errorMessage);
                }
            }
        } catch (error) {
            console.error('Error:', error);
            setError(error.message);
            setShowAlert(true);
        }

        setShowEditModal(false);
        handleSearch();
    };

    const handleTagDelete = (index) => {
        setSelectedCertificate(prevState => {
            const tags = prevState.tags.filter((tag, i) => i !== index);
            return { ...prevState, tags };
        });
    };

    const handleTagAdd = () => {
        if (newTag.trim()) { // ensure the new tag isn't empty or just whitespace
            setSelectedCertificate(prevState => {
                const tags = [...prevState.tags, { name: newTag }];
                return { ...prevState, tags };
            });
            setNewTag(''); // reset the newTag input
        }
    };

    const handleDelete = (certificate) => {
        setSelectedCertificate(certificate);
        setShowDeleteModal(true);
    };

    const handleCreate = () => {
        setSelectedCertificate({
            "name": "",
            "description": "",
            "price": "",
            "duration": "",
            "tags": []
        });
        setShowCreateModal(true);
    };

    const handleCreateConfirm = async () => {
        const token = await getAccessToken();
        if (!token) {
            logout();
            return;
        }

        const requestBody = selectedCertificate;

        const apiUrl = 'http://localhost:8080/api/certificates';

        try {
            const response = await fetch(apiUrl, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(requestBody)
            });
            if (response.status !== 201) {
                const error = await response.json();
                let errorMessage
                if (response.status >= 500) {
                    errorMessage = "Oops! Something went wrong, please try again later.";
                    setError(errorMessage);
                    setShowAlert(true);
                } else {
                    errorMessage = "HTTP error " + response.status + " - " + JSON.stringify(error);
                    throw new Error(errorMessage);
                }
            }
        } catch (error) {
            console.error('Error:', error);
            setError(error.message);
            setShowAlert(true);
        }

        setShowCreateModal(false);
        handleSearch();

    };

    const handleDeleteConfirm = async () => {
        const token = await getAccessToken();
        if (!token) {
            logout();
            return;
        }

        const baseApiUrl = 'http://localhost:8080/api/certificates';
        const apiUrl = `${baseApiUrl}/${selectedCertificate.id}`;

        try {
            const response = await fetch(apiUrl, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            if (response.status !== 204) {
                const error = await response.json();
                let errorMessage
                if (response.status >= 500) {
                    errorMessage = "Oops! Something went wrong, please try again later.";
                    setError(errorMessage);
                    setShowAlert(true);
                } else {
                    errorMessage = "HTTP error " + response.status + " - " + JSON.stringify(error);
                    throw new Error(errorMessage);
                }
            }
        } catch (error) {
            console.error('Error:', error);
            setError(error.message);
            setShowAlert(true);
        }

        setShowDeleteModal(false);
        handleSearch();
    };

    return (
        <div>
            <NavBar showAddButton={true} showUserOptions={true} username={localStorage.getItem('username')} onClick={handleCreate} />
            <Modal show={showDeleteModal} onHide={() => handleCloseModal(() => setShowDeleteModal(false))} >
                <Modal.Header closeButton>
                    <Modal.Title>Delete confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Delete Gift Certificate {selectedCertificate?.name}(id={selectedCertificate?.id}) </p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleCloseModal(() => setShowDeleteModal(false))}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={handleDeleteConfirm} >
                        Yes
                    </Button>
                </Modal.Footer>
            </Modal>
            <EditModal
                show={showEditModal}
                onHide={() => setShowEditModal(false)}
                title="Edit Certificate"
                selectedCertificate={selectedCertificate}
                setSelectedCertificate={setSelectedCertificate}
                handleCloseModal={() => handleCloseModal(setShowEditModal)}
                handleSave={handleEditSave}
                saveButtonName="Save Changes"
                handleTagAdd={handleTagAdd}
                newTag={newTag}
                setNewTag={setNewTag}
                handleTagDelete={handleTagDelete}
            />
            <EditModal
                show={showCreateModal}
                onHide={() => setShowCreateModal(false)}
                title="New Certificate"
                selectedCertificate={selectedCertificate}
                setSelectedCertificate={setSelectedCertificate}
                handleCloseModal={() => handleCloseModal(setShowCreateModal)}
                handleSave={handleCreateConfirm}
                saveButtonName="Create New Certificate"
                handleTagAdd={handleTagAdd}
                newTag={newTag}
                setNewTag={setNewTag}
                handleTagDelete={handleTagDelete}
            />
            <Modal show={showModal} onHide={() => handleCloseModal(() => setShowModal(false))}>
                <Modal.Header closeButton>
                    <Modal.Title>{selectedCertificate?.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Description: {selectedCertificate?.description}</p>
                    <p>Price: {selectedCertificate?.price}</p>
                    <p>Duration: {selectedCertificate?.duration}</p>
                    <p>Create Date: {new Date(selectedCertificate?.createDate).toLocaleDateString()}</p>
                    <p>Last Update Date: {new Date(selectedCertificate?.lastUpdateDate).toLocaleDateString()}</p>
                    <p>Tags: {selectedCertificate?.tags.map(tag => tag.name).join(', ')}</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleCloseModal(() => setShowModal(false))}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
            <Container style={{ paddingTop: '70px' }}>
                <Alert variant="danger" show={showAlert} onClose={() => setShowAlert(false)} dismissible>
                    <Alert.Heading>Error!</Alert.Heading>
                    {error}
                </Alert>
                <Form onSubmit={handleSearchInput}>
                    <Row className="d-flex justify-content-center mb-4">
                        <div style={{ display: 'flex' }}>
                            <Form.Control size="lg" style={{ width: '80%' }} type="text" placeholder="Search" value={search} onChange={e => setSearch(e.target.value)} />
                            <Button size="lg" style={{ width: '20%' }} variant="outline-primary" type="submit">Search</Button>
                        </div>
                    </Row>
                </Form>
                <Row>
                    <Col xs={12}>
                        {loading ? (
                            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '300px' }}>
                                <Spinner animation="border" role="status">
                                    <span className="sr-only"></span>
                                </Spinner>
                            </div>
                        ) : (
                            <Table striped bordered hover variant="dark">
                                <thead>
                                    <tr>
                                        <th onClick={() => handleSort('sortByDate')} className="clickable">
                                            <div className="header-wrapper">
                                                <div className="sorting-icons">
                                                    {sortOrder['sortByDate'] !== 'DESC' && <img src={caretUpFill} alt="caret-up" />}
                                                    {sortOrder['sortByDate'] !== 'ASC' && <img src={caretDownFill} alt="caret-down" />}
                                                </div>
                                                <span>Date</span>
                                            </div>
                                        </th>
                                        <th onClick={() => handleSort('sortByName')} className="clickable">
                                            <div className="header-wrapper">
                                                <div className="sorting-icons">
                                                    {sortOrder['sortByName'] !== 'DESC' && <img src={caretUpFill} alt="caret-up" />}
                                                    {sortOrder['sortByName'] !== 'ASC' && <img src={caretDownFill} alt="caret-down" />}
                                                </div>
                                                <span>Title</span>
                                            </div>
                                        </th>
                                        <th>Tags</th>
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {data && data._embedded && data._embedded.giftCertificates && data._embedded.giftCertificates.length > 0 ? (
                                        data._embedded.giftCertificates.map(item => (
                                            <tr key={item.id}>
                                                <td>{new Date(item.createDate).toLocaleDateString()}</td>
                                                <td>{item.name}</td>
                                                <td>{item.tags.map(tag => tag.name).join(', ')}</td>
                                                <td>{item.description}</td>
                                                <td>{item.price}</td>
                                                <td>
                                                    <div className="button-group">
                                                        <Button variant="outline-primary" className="mx-1" onClick={() => handleView(item)}>View</Button>
                                                        <Button variant="outline-warning" className="mx-1" onClick={() => handleEdit(item)}>Edit</Button>
                                                        <Button variant="outline-danger" className="mx-1" onClick={() => handleDelete(item)}>Delete</Button>
                                                    </div>
                                                </td>
                                            </tr>
                                        ))
                                    ) : (
                                        <tr>
                                            <td colSpan="6">No certificates found.</td>
                                        </tr>
                                    )}
                                </tbody>

                            </Table>
                        )}
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col className=" offset-md-5 text-center">
                        <Pagination size="lg">
                            <Pagination size="lg">
                                <Pagination.First
                                    onClick={(event) => {
                                        handlePagination(links.first);
                                        event.currentTarget.blur();
                                    }}
                                    disabled={!links.first}
                                />
                                <Pagination.Prev
                                    onClick={(event) => {
                                        handlePagination(links.prev);
                                        event.currentTarget.blur();
                                    }}
                                    disabled={!links.prev}
                                />
                                <Pagination.Next
                                    onClick={(event) => {
                                        handlePagination(links.next);
                                        event.currentTarget.blur();
                                    }}
                                    disabled={!links.next}
                                />
                                <Pagination.Last
                                    onClick={(event) => {
                                        handlePagination(links.last);
                                        event.currentTarget.blur();
                                    }}
                                    disabled={!links.last}
                                />
                            </Pagination>
                        </Pagination>
                    </Col>
                    <Col xs="auto">
                        <DropdownButton size="lg" id="items-per-page-dropdown" title={selectedValue} onSelect={handleSelect}>
                            <Dropdown.Item eventKey="10">10</Dropdown.Item>
                            <Dropdown.Item eventKey="20">20</Dropdown.Item>
                            <Dropdown.Item eventKey="50">50</Dropdown.Item>
                        </DropdownButton>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default CertificatesPage;