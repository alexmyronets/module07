import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

function OauthPage() {
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        const query = new URLSearchParams(location.hash.slice(1));
        const idToken = query.get('id_token');
        console.log(idToken);
        localStorage.setItem('access_token', idToken);
        navigate('/certificates');
     }, [location]);
}

export default OauthPage;