import React, { useState } from 'react';
import NavBar from '../components/NavBar';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import { GoogleLogin } from '@react-oauth/google';
import jwtDecode from 'jwt-decode';


function LoginPage() {
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [validated, setValidated] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [error, setError] = useState('');


    const responseMessage = (response) => {
        localStorage.setItem('access_token', response.credential);
        const decodedToken = jwtDecode(response.credential);
        localStorage.setItem('username', decodedToken.name);
        navigate('/certificates');
    };
    const errorMessage = (error) => {
        console.log(error);
    };

    const handleLogin = (e) => {
        e.preventDefault();
        setShowAlert(false);

        const form = e.currentTarget;
        if (form.checkValidity() === false) {
            setValidated(true);
            return
        }

        fetch('http://localhost:8080/api/login', {
            method: 'GET',
            headers: {
                'Authorization': 'Basic ' + btoa(username + ":" + password),
            },
        })
            .then(async response => {
                if (!response.ok) {
                    const error = await response.json();
                    let errorMessage;
                    if (response.status === 401) {
                        errorMessage = "Username or password is incorrect";
                    } else {
                        errorMessage = "HTTP error " + response.status + " - " + error.errorMessage;
                    }
                    throw new Error(errorMessage);
                }
                return response.json();
            })
            .then(data => {
                localStorage.setItem('access_token', data.access_token);
                localStorage.setItem('refresh_token', data.refresh_token);
                localStorage.setItem('username', username);
                navigate('/certificates');
            })
            .catch(err => {
                console.error(err);
                setError(err.message);
                setShowAlert(true);
            });
    };

    return (
        <div>
            <NavBar showAddButton={false} showUserOptions={false} />
            <Container style={{ paddingTop: '70px' }}>
                <Row className="justify-content-center">
                    <Col xs={12} sm={8} md={6} lg={4}>
                        <Alert variant="danger" show={showAlert} onClose={() => setShowAlert(false)} dismissible>
                            <Alert.Heading>Login error!</Alert.Heading>
                            {error}
                        </Alert>
                        <Form className="form-signin mt-4 needs-validation" noValidate validated={validated} onSubmit={handleLogin}>
                            <Form.Floating className="mb-3">
                                <Form.Control
                                    id="floatingInput"
                                    type="text"
                                    placeholder="Enter your username"
                                    value={username}
                                    onChange={e => setUsername(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a username.
                                </Form.Control.Feedback>
                                <label htmlFor="floatingInput">Login</label>
                            </Form.Floating>

                            <Form.Floating className="mb-3">
                                <Form.Control
                                    id="floatingPassword"
                                    type="password"
                                    placeholder="Password"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a password.
                                </Form.Control.Feedback>
                                <label htmlFor="floatingPassword">Password</label>
                            </Form.Floating>
                            <Button className="btn btn-dark w-100 py-2" type="submit">Sign in</Button>

                            <div className="d-flex align-items-center my-3">
                                <hr className="flex-grow-1" />
                                <span className="mx-2">OR</span>
                                <hr className="flex-grow-1" />
                            </div>

                            <Form.Floating className="d-flex justify-content-center align-items-center mb-3 py-2">
                                <GoogleLogin
                                    
                                    theme="filled_black"
                                    width="400"
                                    onSuccess={responseMessage}
                                    onError={errorMessage}
                                    scope="openid"
                                    prompt="select_account"
                                />
                            </Form.Floating>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default LoginPage;
