import { createContext, useContext } from "react";
import { isTokenExpired } from "./hooks/isTokenExpired";

const AuthContext = createContext();

export function useAuth() {
    return useContext(AuthContext);
}

export function AuthProvider({ children }) {
    const API_URL = 'http://localhost:8080/api';

    const refreshTokenFunc = async (refreshToken) => {
        try {
            console.log(JSON.stringify({
                refreshToken
            }));
            const response = await fetch(`${API_URL}/login/refresh-token`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    refreshToken
                })
            });

            if (!response.ok) {
                throw new Error('Refresh token failed');
            }

            const data = await response.json();
            localStorage.setItem('access_token', data.access_token);
            localStorage.setItem('refresh_token', data.refresh_token);
        } catch (err) {
            console.error('Refresh token failed');
            localStorage.clear();
            return;
        }
    }

    const getAccessToken = async () => {
        const accessToken = localStorage.getItem('access_token');
        const refreshToken = localStorage.getItem('refresh_token');

        if (isTokenExpired(accessToken)) {
            console.log('Access token expired');
            if (!isTokenExpired(refreshToken)) {
                console.log('Using refresh token to get new access token');
                await refreshTokenFunc(refreshToken);
                return localStorage.getItem('access_token');
            } else {
                console.log('refresh token expired, need to re-login');
                localStorage.clear();
                return null;
            }
        }
        return accessToken;
    }

    const updateTokens = async () => {
        const accessToken = localStorage.getItem('access_token');
        const refreshToken = localStorage.getItem('refresh_token');

        if (isTokenExpired(accessToken) && !isTokenExpired(refreshToken)) {
            await refreshTokenFunc(refreshToken);
        }
    }

    return (
        <AuthContext.Provider value={{ getAccessToken, updateTokens }}>
            {children}
        </AuthContext.Provider>
    );
}
