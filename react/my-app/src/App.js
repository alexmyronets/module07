import { useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route, useNavigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import LoginPage from './pages/LoginPage';
import CertificatesPage from './pages/CertificatesPage';
import OauthPage from './pages/OauthPage';
import Footer from './components/Footer';
import { AuthProvider } from './AuthProvider';
import { GoogleOAuthProvider } from '@react-oauth/google';

function Home() {
  const navigate = useNavigate();
  useEffect(() => {
    if (localStorage.getItem('access_token')) {
      navigate('/certificates');
    } else {
      navigate('/login');
    }
  }, [navigate]);

  return null;
}

function App() {
  return (
    <GoogleOAuthProvider clientId="1038434610826-m17do5sdur4sutrro439h6duvtvtbt8q.apps.googleusercontent.com" >
      <AuthProvider>
        <Router>
          <div className="app-container">
            <div className="content-grow">
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/certificates" element={<CertificatesPage />} />
                <Route path="/oauth2/callback/google" element={<OauthPage />} />
              </Routes>
            </div>
            <Footer />
          </div>
        </Router>
      </AuthProvider>
    </GoogleOAuthProvider >
  );
}

export default App;
