import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Chip from '../components/Chip';
import { useState } from 'react';

function EditModal({
    show,
    title,
    selectedCertificate,
    setSelectedCertificate,
    handleCloseModal,
    handleSave,
    saveButtonName,
    handleTagAdd,
    newTag,
    setNewTag,
    handleTagDelete
}) {
    const [nameFieldError, setNameFieldError] = useState(null);
    const [descriptionFieldError, setDescriptionFieldError] = useState(null);
    const [priceFieldError, setPriceFieldError] = useState(null);
    const [durationFieldError, setDurationFieldError] = useState(null);
    const [tagInputError, setTagInputError] = useState('');
    const [tagFieldError, setTagFieldError] = useState('');

    const submitForm = async () => {
        if (!validateFields()) {
            return;
        }

        resetValidationErrors();
        handleSave();
    };

    const validateFields = () => {
        let areFieldsValid = true;

        const nameError = validateNameField(selectedCertificate?.name);
        setNameFieldError(nameError);
        if (nameError !== '') {
            areFieldsValid = false;
        }

        const descError = validateDescriptionField(selectedCertificate?.description);
        setDescriptionFieldError(descError);
        if (descError !== '') {
            areFieldsValid = false;
        }

        const priceError = validatePriceField(selectedCertificate?.price);
        setPriceFieldError(priceError);
        if (priceError !== '') {
            areFieldsValid = false;
        }

        const durationError = validateDurationField(selectedCertificate?.duration);
        setDurationFieldError(durationError);
        if (durationError !== '') {
            areFieldsValid = false;
        }

        const tagError = validateTagField(selectedCertificate?.tags);
        if (tagError !== '') {
            setTagFieldError(tagError);
            areFieldsValid = false;
        }

        return areFieldsValid;
    };

    const validateNameField = (name) => {
        if (!name || name.trim() === '') {
            return "Name cannot be blank.";
        }
        if (name.length < 2) {
            return 'Name cannot be shorter than 3 characters.';
        }
        if (name.length > 16) {
            return 'Name cannot be longer than 16 characters.';
        }
        return '';
    };

    const validateDescriptionField = (desc) => {
        if (!desc || desc.trim() === '') {
            return 'Description cannot be blank.';
        }
        if (desc.length < 5) {
            return 'Description cannot be shorter than 5 characters.';
        }
        if (desc.length > 150) {
            return 'Description cannot be longer than 150 characters.';
        }
        return '';
    };

    const validatePriceField = (price) => {
        if (!price) {
            return 'Price cannot be blank.';
        }
        if (isNaN(parseFloat(price))) {
            return 'Price must be a number or float.';
        }
        if (price <= 0) {
            return 'Price must be greater than 0';
        }
        return '';
    };

    const validateDurationField = (duration) => {
        if (!duration) {
            return 'Duration cannot be blank.';
        }
        if (isNaN(parseInt(duration)) || parseInt(duration) < 1) {
            return 'Minimum duration is 1.';
        }
        return '';
    };

    const addTag = () => {
        const inputTagError = validateInputTag(newTag);
        if (inputTagError !== '') {
            setTagInputError(inputTagError);
            return;
        }
        handleTagAdd();
    }

    const validateInputTag = (tag) => {
        if (!tag || tag.trim() === '') {
            return 'Tag cannot be blank.';
        }
        if (tag.length < 3) {
            return 'Tag cannot be shorter than 3 characters.';
        }
        if (tag.length > 15) {
            return 'Tag cannot be longer than 15 characters.';
        }
        return '';
    }

    const validateTagField = (tags) => {
        if (!tags || tags.length === 0) {
            return 'At least one tag should be specified.';
        }
        return '';
    };

    const resetValidationErrors = () => {
        setNameFieldError(null);
        setDescriptionFieldError(null);
        setPriceFieldError(null);
        setDurationFieldError(null);
        setTagFieldError(null);
    };

    return (
        <Modal size="lg" show={show} onHide={() => { handleCloseModal(); resetValidationErrors(); }}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form className="needs-validation">
                    <Row className="mb-3">
                        <Form.Label className="col-sm-2 col-form-label">Name</Form.Label>
                        <Col>
                            <Form.Control
                                className={nameFieldError === null ? '' : nameFieldError ? 'is-invalid' : 'is-valid'}
                                type="text"
                                value={selectedCertificate?.name}
                                onChange={(e) => {
                                    setSelectedCertificate({ ...selectedCertificate, name: e.target.value });
                                    setNameFieldError(null);
                                }}
                            />
                            <Form.Control.Feedback type="invalid">
                                {nameFieldError}
                            </Form.Control.Feedback>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Form.Label className="col-sm-2 col-form-label">Description</Form.Label>
                        <Col>
                            <Form.Control
                                className={descriptionFieldError === null ? '' : descriptionFieldError ? 'is-invalid' : 'is-valid'}
                                as="textarea"
                                rows={3}
                                value={selectedCertificate?.description}
                                onChange={(e) => {
                                    setSelectedCertificate({ ...selectedCertificate, description: e.target.value });
                                    setDescriptionFieldError(null);
                                }}
                            />
                            <Form.Control.Feedback type="invalid">
                                {descriptionFieldError}
                            </Form.Control.Feedback>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Form.Label className="col-sm-2 col-form-label">Price</Form.Label>
                        <Col>
                            <Form.Control
                                className={priceFieldError === null ? '' : priceFieldError ? 'is-invalid' : 'is-valid'}
                                type="number"
                                value={selectedCertificate?.price}
                                onChange={(e) => {
                                    setSelectedCertificate({ ...selectedCertificate, price: e.target.value });
                                    setPriceFieldError(null);
                                }}
                            />
                            <Form.Control.Feedback type="invalid">
                                {priceFieldError}
                            </Form.Control.Feedback>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Form.Label className="col-sm-2 col-form-label">Duration</Form.Label>
                        <Col>
                            <Form.Control
                                className={durationFieldError === null ? '' : durationFieldError ? 'is-invalid' : 'is-valid'}
                                type="number"
                                value={selectedCertificate?.duration}
                                onChange={(e) => {
                                    setSelectedCertificate({ ...selectedCertificate, duration: e.target.value });
                                    setDurationFieldError(null);
                                }}
                            />
                            <Form.Control.Feedback type="invalid">
                                {durationFieldError}
                            </Form.Control.Feedback>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Form.Label column sm="2">Tags</Form.Label>
                        <Col>
                            <Form.Control
                                isInvalid={!!tagInputError}
                                type="text"
                                value={newTag}
                                onChange={(e) => {
                                    setNewTag(e.target.value);
                                    setTagInputError();
                                    setTagFieldError();
                                }}
                                onKeyPress={(e) => {
                                    if (e.key === 'Enter') {
                                        e.preventDefault();
                                        addTag();
                                    }
                                }}
                                placeholder="Add a new tag"
                            />
                            <Form.Control.Feedback type="invalid">
                                {tagInputError}
                            </Form.Control.Feedback>
                        </Col>
                        <Col sm="2">
                            <Button onClick={addTag}>Add Tag</Button>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <div>
                            {tagFieldError && <div className="text-danger">{tagFieldError}</div>}
                            {selectedCertificate?.tags.map((tag, index) => (
                                <Chip key={index} label={tag.name} onDelete={() => handleTagDelete(index)} />
                            ))}
                        </div>
                    </Row>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => { handleCloseModal(); resetValidationErrors(); }}>
                    Close
                </Button>
                <Button variant="primary" onClick={submitForm}>
                    {saveButtonName}
                </Button>
            </Modal.Footer>
        </Modal>);
}

export default EditModal;