import React from 'react';
import Navbar from 'react-bootstrap/Navbar';

const Footer = () => (
  <Navbar bg="dark" variant="dark">
    <Navbar.Toggle />
    <Navbar.Collapse className="justify-content-center">
      <Navbar.Text>
        Oleksandr Myronets 2023 © Weyland-Yutani Corporation
      </Navbar.Text>
    </Navbar.Collapse>
  </Navbar>
);

export default Footer;
