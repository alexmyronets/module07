const Chip = ({ label, onDelete }) => (
    <div style={{
        position: 'relative',
        display: 'inline-block',
        padding: '10px',
        border: '1px solid #ccc',
        borderRadius: '5px',
        margin: '5px'
    }}>
        <span>{label}</span>
        <button
            onClick={onDelete}
            style={{
                position: 'absolute',
                top: '-5px',
                right: '-5px',
                border: 'none',
                background: 'transparent',
                cursor: 'pointer',
                width: '20px',
                height: '20px',
                borderRadius: '50%',
                backgroundColor: '#ccc',
                textAlign: 'center',
                lineHeight: '18px'
            }}
        >
            x
        </button>
    </div>
);

export default Chip;