import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import { useLogout } from '../hooks/useLogout';

function NavBar({ showAddButton, showUserOptions, username, onClick }) {
    const logout = useLogout();
    return (
        <Navbar fixed="top" bg="dark" expand="lg" data-bs-theme="dark">
            <Navbar.Brand style={{ marginLeft: '10px' }} href="/">Admin UI</Navbar.Brand>

            {showAddButton && (
                <Button variant="outline-primary" onClick={onClick}>Add New</Button>
            )}

            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                {showUserOptions && (
                    <Nav>
                        <Navbar.Text style={{ marginRight: '10px' }}>{username}</Navbar.Text>
                        <Button style={{ marginRight: '10px' }} variant="outline-danger" onClick={logout}>Logout</Button>
                    </Nav>
                )}
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavBar;
