var searchForm = document.querySelector(".navbar-search");
searchForm.style.display = "none";

function getProduct() {
    return JSON.parse(localStorage.getItem(decodeURIComponent(new URLSearchParams(window.location.search).get("product"))));
}

function renderProduct(product) {
    // Find 'page-content' div
    let pageContent = document.querySelector('.page-content');

    // Create 'product-container' div
    let productContainer = document.createElement('div');
    productContainer.className = 'product-container';

    // Create 'product-img' img
    let productImg = document.createElement('img');
    productImg.className = 'product-img';
    productImg.src = product.image;
    productImg.alt = product.name;

    // Create 'product-info' div
    let productInfo = document.createElement('div');
    productInfo.className = 'product-info';

    // Create 'product-name' h2
    let productName = document.createElement('h2');
    productName.className = 'product-name';
    productName.textContent = product.name;

    // Create 'product-short-description' p
    let productShortDesc = document.createElement('p');
    productShortDesc.className = 'product-short-description';
    productShortDesc.textContent = product.shortDescription;

    // Create 'product-expires-in' p
    let productExpiresIn = document.createElement('p');
    productExpiresIn.className = 'product-expires-in';
    productExpiresIn.textContent = 'Expires in ' + countExpirationDays(product.validDate) + ' days';

    // Create 'product-footer' div
    let productFooter = document.createElement('div');
    productFooter.className = 'product-footer';

    // Create 'product-price' span
    let productPrice = document.createElement('span');
    productPrice.className = 'product-price';
    productPrice.textContent = '$' + product.price;

    // Create 'add-to-cart-btn' button
    let addToCartBtn = document.createElement('button');
    addToCartBtn.className = 'add-to-cart-btn';
    addToCartBtn.textContent = 'Add to cart';

    // Create 'product-long-description' div
    let productLongDesc = document.createElement('div');
    productLongDesc.className = 'product-long-description';
    productLongDesc.textContent = product.longDescription;

    // Append children to 'product-footer' div
    productFooter.appendChild(productPrice);
    productFooter.appendChild(addToCartBtn);

    // Append children to 'product-info' div
    productInfo.appendChild(productName);
    productInfo.appendChild(productShortDesc);
    productInfo.appendChild(productExpiresIn);
    productInfo.appendChild(productFooter);

    // Append children to 'product-container' div
    productContainer.appendChild(productImg);
    productContainer.appendChild(productInfo);

    // Append children to 'page-content' div
    pageContent.appendChild(productContainer);
    pageContent.appendChild(productLongDesc);
}

function countExpirationDays(expirationDate) {
    let today = new Date();
    let expirationDateObj = new Date(expirationDate);
    console.log(expirationDateObj);
    let diff = expirationDateObj - today;
    let days = Math.floor(diff / (1000 * 60 * 60 * 24));
    return days;
}

renderProduct(getProduct());