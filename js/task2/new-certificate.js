let productsJson = Object.values(localStorage);
let productsParsed = productsJson.map(JSON.parse);
let categories = new Set();

for (let product of productsParsed) {
    categories.add(product.category);
}

var searchForm = document.querySelector(".navbar-search");
searchForm.style.display = "none";

let categoryDropdown = document.getElementById('product-category');

for (let category of categories) {
    let option = document.createElement('option');
    option.textContent = category;
    option.value = category;
    categoryDropdown.appendChild(option);
}

document.getElementById('new-product-form').addEventListener('submit', function (event) {
    event.preventDefault();

    var product = {
        name: document.getElementById('product-name').value,
        category: document.getElementById('product-category').value,
        validDate: document.getElementById('valid-to').value,
        price: parseFloat(document.getElementById('product-price').value),
        shortDescription: document.getElementById('short-description').value,
        longDescription: document.getElementById('long-description').value,
        createdDate: new Date().getTime()
    };

    var fileInput = document.getElementById('product-image');

    var reader = new FileReader();
    reader.onload = function (e) {
        product.image = e.target.result;

        try {
            localStorage.setItem(product.name, JSON.stringify(product));
            alert('Product created successfully!');
        } catch (e) {
            if (e.code === 22) {
                alert('Failed to create product: Storage limit exceeded.');
            } else {
                alert('Failed to create product: Unknown error.');
            }
        }
    }
    reader.readAsDataURL(fileInput.files[0]);

});