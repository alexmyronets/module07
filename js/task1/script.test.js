import test from 'ava';
import * as script from './script.js';

test('Seconds to date test', t => {
    t.deepEqual(script.secondsToDate(31536000), new Date('2021-06-01T00:00:00Z'));
    t.deepEqual(script.secondsToDate(0), new Date('2020-06-01T00:00:00Z'));
    t.deepEqual(script.secondsToDate(86400), new Date('2020-06-02T00:00:00Z'));
});

test('Base 2 converter test', t => {
    t.deepEqual(script.toBase2Converter(5), '101')
    t.deepEqual(script.toBase2Converter(10), '1010')
});

test('substringOccurrencesCounter function', t => {
    t.is(script.substringOccurrencesCounter('a', 'test it'), 0);
    t.is(script.substringOccurrencesCounter('t', 'test it'), 3);
    t.is(script.substringOccurrencesCounter('T', 'test it'), 3);
});

test('repeatingLitters function', t => {
    t.is(script.repeatingLitters('Hello'), 'HHeelloo');
    t.is(script.repeatingLitters('Hello world'), 'HHeelloo  wworrldd');
});

test('redundant function', t => {
    const f1 = script.redundant('apple');
    t.is(f1(), 'apple');
    const f2 = script.redundant('pear');
    t.is(f2(), 'pear');
    const f3 = script.redundant('');
    t.is(f3(), '');
});

test('towerHanoi function', t => {
    t.is(script.towerHanoi(3), 7);
    t.is(script.towerHanoi(4), 15);
});

test('matrixMultiplication function', t => {
    const matrix1 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    const matrix2 = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    t.deepEqual(script.matrixMultiplication(matrix1, matrix2), [[30, 36, 42], [66, 81, 96], [102, 126, 150]]);
});

test('gather function', t => {
    t.is(script.gather("a")("b")("c").order(0)(1)(2).get(), "abc");
    t.is(script.gather("a")("b")("c").order(2)(1)(0).get(), "cba");
    t.is(script.gather("e")("l")("o")("l")("!")("h").order(5)(0)(1)(3)(2)(4).get(), "hello!");
});